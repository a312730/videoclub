FROM node
MAINTAINER Adan Ramirez
WORKDIR /app
COPY . .
RUN npm install
EXPOSE 3000
CMD npm start 
