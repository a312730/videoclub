const express = require('express');

//RESTFULL => GET POST PUT PATCH DELETE
//Modelo = estructura de datos que representa una entidad del mundo real
function list(req, res, next) {
  res.send('Lista de usuarios del sistema: ');
}

function index(req, res, next){
  res.send(`Usuario del sistema con un ID = ${req.params.id}`);
}

function create(req, res, next){
  res.send('crear un usuario nuevo');
}

function replace(req, res, next){
  res.send(`remplazo un usuario con ID = ${req.params.id}`);
}

function edit(req, res, next){
  res.send(`remplazo propiedades de usuario con ID = ${req.params.id}`);
}

function destroy(req, res, next){
  res.send(`elimino usuario del sistema con un ID = ${req.params.id}`);
}

module.exports = {
  list, index, create, replace, edit, destroy
}
